// 1
function divide(a, b) {
   return a / b;
}
console.log("Частка чисел 6 і 2:", divide(6, 2));


// 2
function calculate() {
   let num1;
   let num2;

   do {
       num1 = parseFloat(prompt("Введіть перше число:"));
   } while (isNaN(num1));

   do {
       num2 = parseFloat(prompt("Введіть друге число:"));
   } while (isNaN(num2));

   let operation;
   do {
       operation = prompt("Введіть математичну операцію (+, -, *, /):");
   } while (!["+","-","*","/"].includes(operation));

   calculateOperation(num1, num2, operation);
}

function calculateOperation(num1, num2, operation) {
   let result;
   switch (operation) {
       case '+':
           result = num1 + num2;
           break;
       case '-':
           result = num1 - num2;
           break;
       case '*':
           result = num1 * num2;
           break;
       case '/':
           if (num2 !== 0) {
               result = num1 / num2;
           } else {
               alert("Ділення на нуль неможливе!");
               return;
           }
           break;
   }
   console.log("Результат обчислення:", result);
}

calculate();
